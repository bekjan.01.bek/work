# создать файл homework4
# создать 5 классов
# в 2 из них есть конструктор (__init__)
# в одном  атрибут name в другом age
# в 2 есть по 1 методу
# в последнем классе
# наследовать все классы
# и сделать так чтобы у последнего класса были все методы и атрибуты (name,age)
# в последнем классе сделать age скрытым и добавить к нему property

class Name:
    def __init__(self, name):
        self.name = name


class Age:
    def __init__(self, age):
        self.__age = age

    @property  # property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age):
        if age < 0:
            raise ValueError('Корректный возраст')
        self.__age = age


class Acquaintance:
    def greeting(self):
        return f"Hi {self}"


class Growth:
    def growth(self):
        return f"I'm {self} years old"


class Adios(Name, Age, Acquaintance, Growth):
    def __init__(self, name, age):
        Name.__init__(self, name)
        Age.__init__(self, age)


object = Adios('Eren', 19)
object.greeting()
object.growth()
