# создать новый проект HomeWork
# создать файл hero.py
# и работать внутри него
#
# 1)создать класс SuperHero
# с атрибутом класса people='people'
#
# 2)создать конструктор класса(init) с атрибутами
# name,nickname,superpower,health_points,
# catchphrase.
#
# 3)создать метод который выводит имя героя
#
# 4)создать метод который умножает здоровье героя на 2
#
# 5) cоздать магический!! метод который
# выводит прозвище героя,его суперспособность и его здоровье
#
# 6)создать магический!! метод который считает длину коронной фразы героя
#
# 7)создать объект класса Hero
# и применить все методы которые вы создали


class SuperHero:
    people = 'people'

    def __init__(self, name, nickname,superpower,health_points,catchphrase):
        self.name = name
        self.nickname = nickname
        self.superpower = superpower
        self.health_points = health_points
        self.catchphrase = catchphrase

    def Name(self):
        print(f'\nname => {self.name}\n')

    def mul_health(self):
        self.health_points *= 2
        return self.health_points


    def __str__(self):
        return f'|=|=|=|=|=|=|=|=|=|=|=|=|=|=|\n' \
               f'nickname => {self.nickname}\n' \
               f'superpower => {self.superpower}\n' \
               f'health_points => {self.health_points}\n' \
               f'|=|=|=|=|=|=|=|=|=|=|=|=|=|=|' \


    def __len__(self):
        return len(self.catchphrase.replace(" ", ""))



#health_points учитывались по киновсленной Марвел от 0 до 100.

Hero = SuperHero('Groot', 'Groot', 'Tree', 76, 'I am Groot')
Hero.Name()
Hero.mul_health()
print(f'catchphrase => {Hero.__len__()}\n')
print(f'health multiplication => {Hero.health_points}\n')
print(Hero)

# health_points учитывались по киновсленной Марвел от 0 до 100.

Hero = SuperHero('Groot', 'Groot', 'Tree', 76, 'I am Groot')
Hero.Name()
Hero.mul_health()
print(f'catchphrase => {Hero.__len__()}\n')
print(f'health multiplication => {Hero.health_points}\n')
print(Hero)


# Hero = SuperHero('Питер Паркер', 'Человек-Паук', 'Паучье сила',38 , 'Ваш дружулюный сосед, Человек-Паук')
# Hero.Name()
# Hero.mul_health()
# print(f'catchphrase => {Hero.__len__()}\n')# print(f'health multiplication => {Hero.health_points}\n')
# print(Hero)


# Наследование)
# Создать 2 класса наследованных от класса SuperHero Все эти классы это герои своей местности(воздушные, земные ,космические и тд)
# к каждому классу добавить свойство.# И новые аргументы damage . И  fly который по УМОЛЧАНИЮ будет равен False
# (полиморфизм)
# У каждого класса наследованных от Hero
# Изменить метод который умножал хп героев на 2 теперь он должен возводить его в квадрат и меняет значение параметра fly на True
# создать новый метод который выводит фразу fly in the True_phrase
# создать объекты у новых классов и вызвать новые методы
# создать класс villain наследованный от нового класса изменить  значение  атрибутa people на monster#
# создать метод gen_x который ПОКА ничего не делает
# создать метод crit который возводит в степень урон
# создать объекты для этого класса
# применить метод crit на объект другого класса  у которого есть аргумент damage
# дз принимаю только гитом (не забудьте про gitignore)# Удалить .idea

class Earth_hero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage, fly=False):
        super().__init__(name, nickname, superpower, health_points, catchphrase)
        self.damage = damage
        self.fly = fly

    def mul_health(self):
        self.fly = True
        return self.health_points ** 2


class Air_Hero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage, fly=True):
        super().__init__(name, nickname, superpower, health_points, catchphrase)
        self.damage = damage
        self.fly = fly

    def fly_pr(self):
        if self.fly:
            return " fly in True phrase"


Hero1 = Earth_hero('Уэйд Уилсон', 'Дэдпул', 'Регенирация', 100, 'Яйца в кулак', 99)
Hero2 = Air_Hero('Тони Старк', 'Железный Человек', 'Технология', 63, " I Love you 3000", 79, fly=True)
print(Hero1.mul_health())
print(Hero2.fly_pr())


class Villain(Air_Hero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage, fly):
        super().__init__(name, nickname, superpower, health_points, catchphrase, damage, fly)
        self.people = 'monster'

    def gen_x(self):
        pass

    def crit(self, mp):
        if mp.damage:
            return mp.damage ** 2


villain = Villain('Веном', 'Веном', 'Симбиоз', 70, 'Мы Веном', 77, fly=False)

print(f'|=|=|=|=|=|=|=|=|=|=|=|=|=|=|\n'
      f'Mul: {Hero1.mul_health()}')
print(f'Fly: {Hero2.fly_pr()}')
print(f' Crit Веном: {villain.crit(villain)}\n'
      f'|=|=|=|=|=|=|=|=|=|=|=|=|=|=|')
print(f'Crit Дэдпул: {villain.crit(Hero1)}\n'
      f'|=|=|=|=|=|=|=|=|=|=|=|=|=|=|')
print(f'Crit Железный Человек: {villain.crit(Hero2)}\n'
      f'|=|=|=|=|=|=|=|=|=|=|=|=|=|=|')
